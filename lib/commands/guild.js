
// Guild commands, for those that need different options depending on the server

import checkCommand, { LoadError } from "./check.function.js";
import { readdirSync } from "node:fs";
import importCommand, { validFile } from "./import.function.js";
import { ApplicationCommand } from "discord.js";

const guildCommands = {};
export { guildCommands as commands };
const defaultShouldCreateFor = () => true;


/**
 * Checks if the given command is in a guild.
 * @param {object|string} command The command (or command name).
 * @param {Guild} guild The guild it should be checked for.
 * @returns {boolean}
 */
export function isIn(command, {id}) {
	if(typeof command === "string")
		command = guildCommands[command];
	return command.apiCommands.has(id);
}


/**
 * Loads guild commands.
 * @param {Client} client The Discord.js client
 * @param {string} folder The absolute path of folder where the commands are.
 * @param {function} [middleware] A function to run on the commands once they are loaded.
 */
export async function init(client, folder, middleware) {
	for(const file of readdirSync(folder, {withFileTypes: true}))
	{
		let { name } = file;
		if(!validFile(name) || !file.isFile())
			continue;

		const command = {
			options: [],
			...await importCommand(`${folder}/${name}`),
		};
		name = name.slice(0, -3);
		command.apiCommands = new Map();

		if("shouldCreateFor" in command)
		{
			if(typeof command.shouldCreateFor !== "function")
				throw new LoadError(name, `Guild command 'shouldCreateFor' must be a function, got ${typeof command.shouldCreateFor}.`);
		}
		else
			command.shouldCreateFor = defaultShouldCreateFor;

		if("getOptions" in command && typeof command.getOptions !== "function")
			throw new LoadError(name, `Guild command 'getOptions' must be a function, got ${typeof command.getOptions}.`);

		Object.defineProperty(command, "name", { value: name, writable: false, configurable: false, enumerable: true });
		middleware?.(name, command);
		checkCommand(command);
		guildCommands[name] = command;
	}


	const guilds = client.guilds.cache;
	for(const command of Object.values(guildCommands))
	{
		for(const guild of guilds.values())
		{
			const apiCmd = guild.commands.cache.find(({name}) => name === command.name);
			if(apiCmd)
				command.apiCommands.set(guild.id, apiCmd);
			else
				createCmd(command, guild);
		}
	}


	client.on("guildCreate", ({id, commands}) => {
		commands.set(Object.values(guildCommands)
			.filter(cmd => cmd.shouldCreateFor(id))
			.map(cmd => ({ ...cmd, options: getOptions(cmd, id) }))
		).then(apiCommands => {
			for(const apiCmd of apiCommands.values())
				guildCommands[apiCmd.name]?.apiCommands.set(id, apiCmd);
		}, console.error);
	});

	client.on("guildDelete", ({id}) => {
		for(const {apiCommands} of Object.values(guildCommands))
			apiCommands.delete(id);
	});
}


function getOptions({getOptions, options}, id) {
	return getOptions?.(id) || options || [];
}


/**
 * Create a command for the given server.
 * @param {object|string} command The command (or command name).
 * @param {Guild} guild The guild it should be created in.
 * @param {boolean} skipCheck If true, the command will be created even if shouldCreateFor returns false.
 * @returns {false|Promise<Map>} false if the command should not be created, or a list of ApplicationCommand corresponding to this command, mapped by server id.
 */
export function createCmd(command, {id, commands}, skipCheck = false)
{
	if(typeof command === "string")
		command = guildCommands[command];
	return (skipCheck || command.shouldCreateFor(id))
		&& commands.create({ ...command, options: getOptions(command, id) })
			.then(apiCmd => command.apiCommands.set(id, apiCmd), createFailed);
}

/**
 * Updates a command for the given server, creating or deleting it if needed. Always obeys shouldCreateFor.
 * @param {object|string} command The command (or command name).
 * @param {Guild} guild The guild it should be updated for.
 * @param {boolean} createIfNotExists If true, the command will be created if it does not exist.
 * @returns {false|Promise<ApplicationCommand>} false if the command should not be created, or a Promise that resolves when the command is updated.
 */
export function updateCmd(command, guild, createIfNotExists = true)
{
	const {id, commands} = guild;
	if(typeof command === "string")
		command = guildCommands[command];
	let apiCmd = command.apiCommands.get(id);
	if(!apiCmd)
	{
		apiCmd = commands.cache.find(({name}) => name === command.name);
		command.apiCommands.set(id, apiCmd);
	}

	if(!command.shouldCreateFor(id) && apiCmd)
	{
		command.apiCommands.delete(id);
		return apiCmd.delete();
	}
	else
	{
		const cmdData = { ...command, options: getOptions(command, id) };
		if(apiCmd)
			return apiCmd.edit(cmdData).catch(err => {
				if(err.status === 404)
					return createCmd(command, guild, true);
				throw err;
			});
		else if(createIfNotExists)
			return commands.create(cmdData).then(apiCmd => command.apiCommands.set(id, apiCmd), createFailed);
		else
			throw new Error(`Tried to update command ${command.name} for guild ${guild}, but the API command couldn't be found.`);
	}
}

/**
 * Delete a command for the given server. Ignores shouldCreateFor.
 * @param {object|string} command The command (or command name).
 * @param {Guild} guild The guild it should be updated for.
 * @returns {?Promise<ApplicationCommand>}
 */
export function deleteCmd(command, {id, commands})
{
	if(typeof command === "string")
		command = guildCommands[command];
	const apiCmd = command.apiCommands.get(id) || commands.cache.find(({name}) => name === command.name);
	command.apiCommands.delete(id);
	return apiCmd?.delete();
}



const failed = new Set();

function createFailed(err)
{
	if(!err.message.includes("daily application command creates"))
		throw err;

	const guildId = err.url.match(/guilds\/([0-9]+)\/commands/)?.[1];
	if(!guildId)
		throw err;
	if(failed.has(guildId))
		return;

	failed.add(guildId);
	setTimeout(failed.delete.bind(failed, guildId), 86400_000); // 24h
	err.guildId = guildId;
	throw err;
}
