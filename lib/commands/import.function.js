
import { pathToFileURL } from "node:url";

/**
 * Imports a command from a file.
 * @param {string} path The path of the command file
 * @returns {object} The command object
 */
export async function importCommand(path)
{
	const command = await import(pathToFileURL(path).href);
	return command.default?.description ? command.default : command;
}

/**
 * Checks whether a file can be a command.
 * @param {string} fileName 
 * @returns {boolean}
 */
export function validFile(fileName)
{
	return fileName[0] !== "~" && fileName.endsWith(".js") && !fileName.includes("#");
}

export default importCommand;