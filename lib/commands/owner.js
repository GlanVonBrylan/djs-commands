
import enums from "../enums.js";
const { SUBCOMMAND, SUBCOMMAND_GROUP } = enums;
import checkCommand, { LoadError, NAME_REGEX } from "./check.function.js";
import importCommand, { validFile } from "./import.function.js";
import { readdirSync } from "node:fs";

export const command = {};
export const commands = {};

/**
 * Loads owner comands.
 * @param {string} parentFolder The absolute path of the parent folder to owner commands.
 * @param {object} data Data about the command
 	* @param {string} data.name The command name. Must be a subfolder under parentFolder. Default: "owner"
 	* @param {string} data.description The command description. Default: "Execute an owner command"
 	* @param {bigint|"0"} data.defaultMemberPermissions Default permissions required to execute the command. Default: "0"
 * @returns {object} the command object, ready to be registered.
 */
export async function load(
	parentFolder,
	{ name, description = "Execute an owner command", defaultMemberPermissions = "0" },
) {
	if(!name)
		name = "owner";
	else if(!NAME_REGEX.test(name))
		throw new Error(`Owner subfolder must have a valid command name; got '${name}'`);

	const folder = `${parentFolder}/${name}`;
	const ownerCmdFiles = readdirSync(folder).filter(validFile);
	if(!ownerCmdFiles)
		return false;

	for(const cmd of ownerCmdFiles)
	{
		const name = cmd.slice(0, -3);
		const command = {
			type: SUBCOMMAND,
			...await importCommand(`${folder}/${cmd}`),
			name,
		};
		if(command.type !== SUBCOMMAND && command.type !== SUBCOMMAND_GROUP)
			throw new LoadError(name, `Owner commands can only have the SUBCOMMAND or SUBCOMMAND_GROUP type.`);
		checkCommand(command);
		commands[name] = command;
	}

	return Object.assign(command, {
		name, description, defaultMemberPermissions,
		options: Object.values(commands),
		commands,
		run: inter => commands[inter.options.getSubcommand()].run(inter),
	});
}
