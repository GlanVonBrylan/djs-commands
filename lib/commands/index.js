
/** Information expected from each command file:
 * description
 * run
 ** Optional:
 * integrationTypes
 * contexts
 * defaultMemberPermissions
 * options
 * autocomplete
 */

import { readdirSync, existsSync } from "node:fs";
var skipDebug = true;
var defaultManager, root, middleware, foldersAreGroups;

import checkCommand, { LoadError } from "./check.function.js";
import enums from "../enums.js";
const { SUBCOMMAND, SUBCOMMAND_GROUP } = enums;

export const commands = {};
export const specialFolders = [];

export var DEFAULT_DM_PERMISSION = false;
export function setDefaultDmPermission(permission) {
	DEFAULT_DM_PERMISSION = !!permission;
}


import importCommand, { validFile } from "./import.function.js";
import typeFail from "../typeFail.js";


/**
 * Load all the commands to Discord.
 * @param {Client} client The Discord.js client
 * @param {string} folder The absolute path of folder where the commands are.
 * @param {object} [options] Additional options
	 * @param {boolean|string} [options.debug] Enable debug mode (all commands defined as guild commands; if debug is a server id, that server is used, otherwise the first one in the cache is used)
	 * @param {Guild} [options.allAsGuild] If defined, all commands will be defined as guild commands of this guild.
	 * @param {boolean} [options.autoSubCommands] If true, folders will be treated as subcommand groups.
	 * @param {function} [options.middleware] A function to run on the commands just before they are sent to Discord.
 * @returns {Promise <Collection <Snowflake, ApplicationCommand>>}
 */
export function init(client, folder, { debug = false, allAsGuild = null, autoSubCommands, middleware: _middleware } = {})
{
	skipDebug = !debug;
	root = folder;
	middleware = _middleware;
	foldersAreGroups = autoSubCommands;
	const { commands: commandManager } = allAsGuild || client.application;
	defaultManager = commandManager;
	return loadFolder(folder, commandManager);
}


async function loadFolder(path, commandManager)
{
	const subfolder = path.substring(root.length + 1);

	for(const file of readdirSync(path, {withFileTypes: true}))
	{
		const { name } = file;
		if(name[0] === "~" && (name !== "~debug" || skipDebug)
			|| name.includes("#")
			|| specialFolders.includes(name))
			continue;

		if(name.endsWith(".js") && file.isFile())
			await load(name, subfolder);
		else if(file.isDirectory())
		{
			if(foldersAreGroups && name !== "~debug")
				await createCommandGroup(name);
			else
				loadFolder(`${path}/${name}`);
		}
	}

	if(commandManager)
		return commandManager.set(Object.values(commands));
}


const readonly = { writable: false, configurable: false, enumerable: true };

export async function load(name, subfolder = "")
{
	if(typeof name !== "string")
		throw typeFail(name, "name", "string");

	if(name.endsWith(".js"))
		name = name.slice(0, -3);

	if(commands[name])
	{
		if(commands[name].subfolder !== subfolder)
			throw new LoadError(name, `Can't load command ${name} of subfolder "${subfolder}", it already exists in subfolder "${commands[name].subfolder}"`);
	}

	const command = {
		options: [],
		dmPermission: DEFAULT_DM_PERMISSION,
		...await importCommand(`${root}/${subfolder}/${name}.js`),
		name, subfolder
	};
	Object.defineProperties(command, { name: readonly, subfolder: readonly });
	middleware?.(name, command);
	checkCommand(command);
	return commands[name] = command;
}


async function createCommandGroup(cmdName)
{
	const path = `${root}/${cmdName}`;
	const cmd = existsSync(`${path}/~info.js`)
		? { ...await importCommand(`${path}/~info.js`) }
		: { description: `/${cmdName}` };
	const options = [];
	const subcommands = {};
	const subcommandGroups = {};
	Object.assign(cmd, {
		name: cmdName, options, subcommands, subcommandGroups,
		run: runCommandGroup, autocomplete: commandGroupAutocomplete,
	});

	for(const file of readdirSync(path, {withFileTypes: true}))
	{
		let { name } = file;
		if(!validFile(name) && !file.isDirectory())
			continue;

		if(file.isDirectory())
		{
			const group = await createSubCommandGroup(cmdName, name);
			options.push(group);
			subcommandGroups[name] = group;
		}
		else if(name.endsWith(".js"))
		{
			name = name.slice(0, -3);
			const subCmd = {
				...await importCommand(`${path}/${name}.js`),
				name,
				type: SUBCOMMAND,
			};
			if(typeof subCmd.run !== "function")
				throw new LoadError(cmdName, `Subcommand ${name} is missing a 'run' function.`);

			subcommands[name] = subCmd;
			options.push(subCmd);
		}
	}

	checkCommand(cmd);
	commands[cmdName] = cmd;
	return cmd;
}


async function createSubCommandGroup(parent, groupName)
{
	const path = `${root}/${parent}/${groupName}`;
	const group = existsSync(`${path}/~info.js`)
		? { ...await importCommand(`${path}/~info.js`) }
		: { description: `/${parent} ${groupName}` };
	const options = [];
	const subcommands = {};
	Object.assign(group, { name: groupName, type: SUBCOMMAND_GROUP, options, subcommands });
	const type = SUBCOMMAND;

	for(const file of readdirSync(path, {withFileTypes: true}))
	{
		let { name } = file;
		if(!validFile(name))
			continue;

		if(file.isDirectory())
			throw new LoadError(parent, `Cannot have a subcommand group inside another subcommand group (in '${groupName}')`);

		if(name.endsWith(".js"))
		{
			name = name.slice(0, -3);
			const subCmd = {
				...await importCommand(`${path}/${name}.js`),
				name, type,
			};
			if(typeof subCmd.run !== "function")
				throw new LoadError(name, `Subcommand ${groupName}/${name} is missing a 'run' function.`);

			subcommands[name] = subCmd;
			options.push(subCmd);
		}
	}

	return group;
}


function runCommandGroup(interaction)
{
	getSubcommand(this, interaction).run(interaction);
}

function commandGroupAutocomplete(interaction)
{
	getSubcommand(this, interaction).autocomplete(interaction);
}

function getSubcommand(commandGroup, {options})
{
	const group = options.getSubcommandGroup();
	const subcmd = options.getSubcommand();
	let subcommands;
	if(group)
	{
		if(group in commandGroup.subcommandGroups)
			subcommands = commandGroup.subcommandGroups[group].subcommands;
		else
			throw new Error(`Received unknown subcommand group: '/${commandGroup.name} ${group}'`);
	}
	else
		subcommands = commandGroup.subcommands;

	if(subcmd in subcommands)
		return subcommands[subcmd];
	else
		throw new Error(`Received unknown subcommand: '/${commandGroup.name} ${group || ""} ${subcmd}'`);
}
