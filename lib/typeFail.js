/**
 * Creates an error explaining how wrong the type of an argument is.
 * @param {*} item The argument
 * @param {string} name The argument's name
 * @param {string} requirement What is should be
 * @returns {TypeError}
 */
export function typeFail(item, name, requirement) {
	return new TypeError(`'${name}' must be a ${requirement} (received ${
        item === null ? "null"
        : typeof item !== "object" ? typeof item
        : `${item.constructor.name} object`
	})`);
}

export default typeFail;