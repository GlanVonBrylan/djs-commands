
import { InteractionType } from "discord.js";
const { 
	ApplicationCommandAutocomplete: APPLICATION_COMMAND_AUTOCOMPLETE,
	ApplicationCommand: APPLICATION_COMMAND,
} = InteractionType;

import { commands } from "./commands/index.js";

export default async function handleInteraction(interaction)
{
	const { type } = interaction;
	if(type !== APPLICATION_COMMAND && type !== APPLICATION_COMMAND_AUTOCOMPLETE)
		return;

	const command = commands[interaction.commandName];
	if(!command)
		console.error(`Received unknown command: ${interaction.commandName}`);
	else if(interaction.type === APPLICATION_COMMAND)
		command.run(interaction);
	else if(typeof command.autocomplete === "function")
		command.autocomplete(interaction);
	else
		console.error(`Received autocomplete interaction for a command without autocomplete (${command.name})`);
};