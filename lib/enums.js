
import {
	ChannelType,
	ApplicationCommandType,
	ApplicationCommandOptionType,
	ComponentType,
	ButtonStyle,
	TextInputStyle,
	ApplicationIntegrationType, InteractionContextType,
} from "discord.js";

export const ALL_TEXT_CHANNEL_TYPES = [
	ChannelType.GuildText,
	ChannelType.PublicThread,
	ChannelType.PrivateThread,
	ChannelType.GuildAnnouncement,
	ChannelType.AnnouncementThread,
];

export const ALL_INTEGRATION_TYPES = [
	ApplicationIntegrationType.GuildInstall,
	ApplicationIntegrationType.UserInstall,
];

export const ALL_CONTEXTS = [
	InteractionContextType.Guild,
	InteractionContextType.BotDM,
	InteractionContextType.PrivateChannel,
];
	

export const exports = {
	ALL_TEXT_CHANNEL_TYPES,
	ALL_INTEGRATION_TYPES,
	ALL_CONTEXTS, BOT_DM: InteractionContextType.BotDM,
};
export default exports;

exportEnums(ApplicationCommandType);
exportEnums(ApplicationCommandOptionType);
exportEnums(ComponentType);
exportEnums(ButtonStyle);
exportEnums(TextInputStyle);


function toSnakeCase(str) {
	return str.replace(/(?!^)[A-Z]/g, letter => `_${letter}`).toUpperCase();
}

function exportEnums(enumeration) {
	for(const [key, bitfield] of Object.entries(enumeration))
		exports[toSnakeCase(key)] = bitfield;
}
