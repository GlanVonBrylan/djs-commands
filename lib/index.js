
import { commands, specialFolders, init, setDefaultDmPermission } from "./commands/index.js";
import * as guildCommands from "./commands/guild.js";
import enums from "./enums.js";
import handleInteraction from "./interactionCreate.js";

const guildExport = { ...guildCommands };
delete guildExport.init;
export { enums, commands, guildExport as guildCommands };

import { statSync } from "node:fs";
import { resolve } from "node:path";
import { Client } from "discord.js";
import typeFail from "./typeFail.js";

export default loadCommands;
/**
 * @param {Client} client The Discord client
 * @param {object} [options] Options
   * @param {string} [options.folder] Defaults to "commands". The folder where your commands are.
   * @param {object} [options.ownerCommand] Details on your owner command
     * @param {string} options.ownerCommand.name 
     * @param {string} [options.ownerCommand.description]
     * @param {bigint|"0"} [options.ownerCommand.defaultMemberPermissions]
   * @param {string} [options.ownerServer] Owner server's id
   * @param {boolean|string} [options.singleServer] Whether this bot will only be in one server
   * @param {boolean} [options.autoSubCommands] Whether a subfolder of commands will become one command with subcommands
   * @param {boolean} [options.debug] Set to true in developmenet, false or left out in production
   * @param {boolean} [options.makeEnumsGlobal] Whether to make SNAKE_CASE enum values global
   * @param {boolean} [options.defaultDmPermission] Whether commands can be used in DMs by default
   * @param {function} [options.middleware] A function to call with each command before registering them
 * @returns {Promise<Collection<Snowflake, ApplicationCommand>>} The commands
 */
export async function loadCommands(client, {
	folder = "commands",
	ownerCommand = { name: "owner" },
	ownerServer, singleServer,
	autoSubCommands = true,
	debug = false,
	makeEnumsGlobal = false,
	defaultDmPermission,
	middleware
} = {})
{
	if(!(client instanceof Client))
		throw typeFail(client, "client", "Discord.js Client");

	if(typeof folder !== "string" || !statSync(folder).isDirectory())
		throw new TypeError("'folder' must be a path to a folder");

	if(middleware && typeof middleware !== "function")
		throw typeFail(middleware, "middleware", "function");

	if(defaultDmPermission)
		setDefaultDmPermission(true);

	if(folder.endsWith("/") || folder.endsWith("\\"))
		folder = folder.slice(0, -1);

	if(!folder.startsWith("/") && !folder.match(/^[A-Z]:/))
		folder = resolve(folder);

	if(makeEnumsGlobal)
		Object.assign(global, enums);

	if(debug)
	{
		if(!singleServer)
			singleServer = ownerServer
				? await client.guilds.fetch(ownerServer)
				: client.guilds.cache.first();
	}
	if(singleServer)
	{
		singleServer = singleServer === true
			? client.guilds.cache.first()
			: await client.guilds.fetch(singleServer);
		ownerServer = singleServer;
	}

	let ownerCmd;
	if(ownerServer)
	{
		if(typeof ownerCommand === "string")
			ownerCommand = { name: ownerCommand };
		else if(typeof ownerCommand !== "object")
			throw typeFail(ownerCommand, "ownerCommand", "string or an object");

		const ownerCmdName = ownerCommand.name;

		if(ownerSubfolderExists(ownerCmdName))
		{
			specialFolders.push(ownerCmdName);
			const { load } = await import("./commands/owner.js");
			ownerCmd = await load(folder, ownerCommand);
			if(singleServer)
				commands[ownerCmdName] = ownerCmd;
			else
			{
				ownerServer = await client.guilds.fetch(ownerServer);
				ownerServer.commands.set([ownerCmd]);
			}
		}
	}

	const load = init(client, folder, {debug, allAsGuild: singleServer, middleware, autoSubCommands});
	if(ownerCmd && !singleServer)
		load.then(() => commands[ownerCmd.name] = ownerCmd);

	if(statSync(`${folder}/~guild`, {throwIfNoEntry: false})?.isDirectory())
		load.then(async () => {
			await guildCommands.init(client, `${folder}/~guild`, middleware);
			Object.assign(commands, guildCommands.commands);
		});

	client.on("interactionCreate", handleInteraction);

	return load;


	function ownerSubfolderExists(name) {
		if(name === "owner")
			return statSync(`${folder}/owner`, {throwIfNoEntry: false})?.isDirectory();
		if(!statSync(`${folder}/${name}`).isDirectory())
			throw new Error("Owner command must not be a file but a subfolder with subcommand files.");
		return true;
	}
};
