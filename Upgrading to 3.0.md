
There are only 2 important changes when upgrading to djs-commands 3:

# ES6 module

The package now uses ES6 modules, instead of NodeJS's default CommonJS. This will require small changes for your command files:

**Before:**
```js
exports.description = "Reply with pong";
exports.run = (interaction) => interaction.reply("Pong!");
```
**3.x:**
```js
export const description = "Reply with pong";
export function run(interaction) {
    interaction.reply("Pong!");
}
```

## Special folders now start with #

Due to the way ES6 modules are imported, **all** files and folders containing a sharp sign `#` will now be ignored.

Special folders now start with a tilde `~` instead, for example you will have to rename `#guild` to `~guild`, `#info.js` to `~info.js`, etc.

## No more reload

The ES6 module cache is hidden and immutable. This means hot-reloading is impossible.

All `reload` function have thus been removed.